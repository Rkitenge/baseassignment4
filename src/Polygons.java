import java.util.Scanner;
/**
 * This program solves the areas and circumferences of polygons
 *
 * @author Raissa Kitenge
 * @Date: 11/13/2020
 */
public class Polygons
{

    public static void main(String[] args)
    {

        double sideA;
        double b;
        double c;
        double circumferenceOfTriangle;
        double pOfTrian;
        double aOfTrian;
        double A;
        double B;
        double C;

        Scanner scnr = new Scanner(System.in);
        // The scanner is important. Makes a new object of the scanner class and store it in the variable input.
        // System.in is calling the constructor of the class to read from the standard input.

        System.out.println("Welcome to my awesome math helper program");
        System.out.println("-----------------------------------------");

        System.out.print("Please enter the first side : ");
        sideA = scnr.nextDouble(); // Reads the numerical value of the data type double
        System.out.print("Please enter the second side : ");
        b = scnr.nextDouble();
        System.out.print("Please enter the side length of the third side : ");
        c = scnr.nextDouble();
        System.out.printf("The three side  lengths of the triangle are %.2f, %.2f, %.2f \n", sideA, b, c);

        A = (Math.pow(sideA,2) + Math.pow(b,2) - Math.pow(c,2)) / (2 * sideA * b);
        B = (Math.pow(b,2) + Math.pow(c,2) - Math.pow(sideA,2)) / (2 * b * c);
        C = (Math.pow(sideA,2) + Math.pow(c,2) - Math.pow(b,2)) / (2 * sideA * c);

        circumferenceOfTriangle = sideA + b + c;
        pOfTrian = (sideA + b + c) / 2;
        aOfTrian = Math.sqrt(pOfTrian * (pOfTrian - sideA) * (pOfTrian - b) * (pOfTrian - c));.

        System.out.printf("First angle : %.2f\n" ,Math.toDegrees(Math.acos(A)));
        System.out.printf("Second angle : %.2f\n" ,Math.toDegrees(Math.acos(B)));
        System.out.printf("Third angle : %.2f\n" , Math.toDegrees(Math.acos(C)));
        System.out.printf("The area of triangle : %.2f square units\n", aOfTrian);
        System.out.printf("The circumference of triangle : %.2f\n", circumferenceOfTriangle);
        System.out.println("-----------------------------------------------------------------------------------");

        double sPen;
        double cOfPen;
        double aOfPen;
        double apOfPen;

        System.out.println("Pentagon (5 sided polygon) area and circumference");
        System.out.println("-------------------------------------------------");
        System.out.print("Enter the side of pentagon: ");
        sPen = scnr.nextDouble();
        System.out.printf("The side of pentagon : %.2f\n" ,sPen);

        cOfPen = 5 * sPen;
        apOfPen = (sPen /2) / Math.tan(2*(Math.PI/10));
        aOfPen = (cOfPen * apOfPen) /2;

        System.out.printf("The circumference of pentagon: %.2f\n",cOfPen);
        System.out.printf("The area of pentagon (with side length %.2f): %.2f square units\n",sPen,aOfPen);
        System.out.println("-----------------------------------------------------------------------------------");

        double sHex;
        double cOfHex;
        double aOfHex;

        System.out.println("Hexagon (6 sided polygon) area and circumference");
        System.out.println("------------------------------------------------");
        System.out.print("Enter the side of hexagon: ");
        sHex = scnr.nextDouble();
        System.out.printf("The side of hexagon: %.2f\n" ,sHex);

        cOfHex = 6 * sHex;
        aOfHex = (3* Math.sqrt(3) * Math.pow(sHex,2)) / 2;

        System.out.printf("The circumference of the regular hexagon: %.2f\n" ,cOfHex);
        System.out.printf("The area of hexagon (with side length %.2f): %.2f square units\n" ,sHex,aOfHex);
        System.out.println("-----------------------------------------------------------------------------------");

        double sHept;
        double cOfHept;
        double aOfHept;
        double apOfHept;

        System.out.println("Heptagon (7 sided polygon) area and circumference");
        System.out.println("-------------------------------------------------");
        System.out.print("Enter the side of heptagon: ");
        sHept = scnr.nextDouble();
        System.out.printf("The side of heptagon: %.2f\n" ,sHept);

        cOfHept = 7 * sHept;
        apOfHept = (sHept/2) / Math.tan(2*(Math.PI/14));
        aOfHept = (cOfHept * apOfHept) /2;

        System.out.printf("The circumference of the regular heptagon: %.2f\n" ,cOfHept);
        System.out.printf("The area of the regular heptagon (with side length %.2f): %.2f square units\n",sHept,aOfHept);
        System.out.println("-----------------------------------------------------------------------------------");

        double sOct;
        double cOfOct;
        double aOfOct;

        System.out.println("Octagon (8 sided polygon) area and circumference");
        System.out.println("------------------------------------------------");
        System.out.print("Enter the side of octagon: ");
        sOct = scnr.nextDouble();
        System.out.printf("The side of octagon: %.2f\n" ,sOct);

        cOfOct = 8 * sOct;
        aOfOct = 2 * Math.pow(sOct,2) * (1+ Math.sqrt(2));

        System.out.printf("The circumference of octagon: %.2f,\n", cOfOct);
        System.out.printf("The area of  octagon (with side length %.2f): %.2f square units\n",sOct,aOfOct);
        System.out.println("-----------------------------------------------------------------------------------");

        double sNon;
        double cOfNon;
        double aOfNon;
        double apOfNon;

        System.out.println("Nonagon (9 sided polygon) area and circumference");
        System.out.println("-----------------------------------------");
        System.out.print("Enter the side of Nonagon: ");
        sNon = scnr.nextDouble();
        System.out.printf("The side of nonagon: %.2f\n" ,sNon);

        cOfNon = 9 * sNon;
        apOfNon = (sNon / 2) / Math.tan(2*(Math.PI/18));
        aOfNon = (cOfNon * apOfNon) /2;

        System.out.printf("The circumference of the regular Nonagon: %.2f\n",cOfNon);
        System.out.printf("The area of the regular Nonagon (with side length %.2f): %.2f square units\n",sNon,aOfNon);
        System.out.println("-----------------------------------------------------------------------------------");

        double sDec;
        double cOfDec;
        double aOfDec;

        System.out.println("Decagon (10 sided polygon) area and circumference");
        System.out.println("-------------------------------------------------");
        System.out.print("Enter the side of Decagon: ");
        sDec = scnr.nextDouble();
        System.out.printf("The side of decagon: %.2f\n" ,sDec);

        cOfDec = 10 * sDec;
        aOfDec = (5 * Math.pow(sDec,2) * Math.sqrt(5 + 2*Math.sqrt(5))) /2;

        System.out.printf("The circumference of Decagon: %.2f\n", cOfDec);
        System.out.printf("The area of Decagon (with side length %.2f): %.2f square units\n",sDec,aOfDec);
        System.out.println("-----------------------------------------------------------------------------------");

    }
}
